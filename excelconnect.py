import logging
import xlrd

LOG  = logging.getLogger()


class ExcelHandler(object):
    
    def __init__(self, path_name, worksheet_name):

        self.path = path_name
        self.worksheet = worksheet_name

    def read_data(self):
        datenfelder =   [
                        'processName',
                        'currentStatus',
                        'externalId'
                        ]
        try: 
            wb = xlrd.open_workbook(self.path, on_demand=True)
            LOG.info("excel workbook loaded")
        except Exception as e:
            LOG.error("could not load excel workbook! Reason: {}".format(str(e)))
        ws = wb.sheet_by_name(self.worksheet)

        outdata = []
        first_row = []
        for row in range(1, ws.nrows):
            record = {}
            for col in range(ws.ncols):
                if ws.cell_value(0, col) in datenfelder:
                    record[ws.cell_value(0, col)] = ws.cell_value(row, col)
            if record['externalId']:
                record['externalId'] = int(record['externalId'])
                outdata.append(record)
        nrows = len(outdata)
        LOG.info("{} relevant data rows found".format(nrows))
        return outdata





