import argparse
import myConfig
import pprint


def show_all():
    con = myConfig.get_all()
    pp.pprint(con)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="See and change settings for _update_anmeldestatus ")
    parser.add_argument("--showall", help="show all current settings", action="store_true")
    parser.add_argument("--change", help="change setting: <name> <value>", nargs=2, type=str)
    args = parser.parse_args()

    pp = pprint.PrettyPrinter(indent=4)

    if args.showall:
        show_all()
    elif args.change:
        key = args.change[0]
        value = args.change[1]
        res = myConfig.get(key)
        if not res:
            print("Setting not found, check again")
        else:
            myConfig.update(key, value)
            show_all()
    else:
        print("no option choosen")

    #done
    ui = input("\nPress ANY KEY to exit \n")
