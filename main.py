#Author: Robert Mieth

import logging

import excelconnect as xlxcon
import googleconnect as goocon
import myConfig


XLSWB = myConfig.get('XlsWorkbook')
XLSWS = myConfig.get('XlsWorksheet')
GWS = myConfig.get('GoogleTable')
SCOL = myConfig.get('WiMColumn_Google')

if __name__ == "__main__":
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('[%(levelname)-6s] [%(funcName)-15s] %(message)s'))
    log = logging.getLogger()
    log.addHandler(handler)
    log.setLevel(logging.INFO)


    # Starting up
    print("")
    print(" *****----------------------------------------------*****")
    print(" *****            Update WIM-Anmeldestatus          *****")
    print(" *****                                              *****")
    print(" *****          SOLANDEO - V1.1 - 2016-08-29        *****")
    print(" *****----------------------------------------------*****")
    print("")

    #Get Excel Data
    excel = xlxcon.ExcelHandler(XLSWB, XLSWS)
    res = excel.read_data()

    #Prepare data
    google = goocon.GoogleHandler(GWS)
    google.set_search_column(SCOL)
    datalist = google.get_data_list()
    workingdata = []
    for d in res:
        for t in datalist:
            if str(d['externalId']) == str(t[0]):
                d['isState'] = t[1]
                workingdata.append(d)
    temp = []
    for d in workingdata:
        if  d['processName'] == 'Messstellenbeginn':
            if d['isState'] == 'Anmeldung läuft':
                if  d['currentStatus'] == 'UTILMD_IN':
                    d['targetState'] = "Bestätigt"
                elif d['currentStatus'] == 'APERAK Empfangen':
                    d['targetState'] = "Gescheitert"
                else:
                    d['targetState'] = False
            elif (d['isState'] == "") or (d['isState'] == "Gescheitert"):
                if d['currentStatus'] == 'UTILMD_OUT':
                    d['targetState'] = "Anmeldung läuft"
                else:
                    d['targetState'] = False
            else:
                d['targetState'] = False
        else:
            d['targetState'] = False
        temp.append(d)
    workingdata = temp
    ntodo = 0
    for d in workingdata:
        if d['targetState']: ntodo+=1
    nice_grammar = "rows"
    if ntodo == 1: nice_grammar = "row"
    log.info("{} data ".format(ntodo) + nice_grammar + " found for update")

    #Write Data
    if ntodo > 0:
        ndone = 0
        ldone = []
        for d in workingdata:
            if d['targetState'] and d['externalId']:
                ndone+=1
                log.info("updating data row {} of {}".format(ndone, ntodo))
                google.update_field(d['externalId'], d['targetState'])
                ldone.append(d['externalId'])

        log.info("the following contracts have been updated:")
        for m in ldone:
            print("                             " + str(m))
    else:
        log.info("nothing to update")

    #done
    ui = input("\nPress ANY KEY to exit \n")
    log.info("bye")