import logging

from oauth2client import tools
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
import argparse

import gspread

LOG = logging.getLogger()


class GoogleHandler(object):

    def __init__(self, worksheet):
        #set up attributes
        self.worksheet_name = worksheet
        self.cred_path = "system\iba_google_credential"
        self.client_secret = "system\client_secret.json"
        self.storage = Storage(self.cred_path)
        self.redirect_uri='urn:ietf:wg:oauth:2.0:oob'
        self.scope = 'https://spreadsheets.google.com/feeds https://docs.google.com/feeds'
        self.error_flag = False
        
        #get credentials
        self.credentials = self.storage.get()
        try:
            token = self.credentials.get_access_token
            if self.credentials.access_token_expired:
                self.credentials = self.obtain_new_credential()
        except:
            self.credentials = self.obtain_new_credential()    
        LOG.info("google credentials ok")

        #set worksheet
        gs = gspread.authorize(self.credentials)
        sheet = gs.open(self.worksheet_name)
        wks = sheet.sheet1
        self.worksheet = wks

    def obtain_new_credential(self):
        LOG.info("current credentials outdated, obtaining new one")
        parser = argparse.ArgumentParser(parents=[tools.argparser])
        flags = parser.parse_args()

        flow = flow_from_clientsecrets(self.client_secret, scope=self.scope, redirect_uri=self.redirect_uri)
        credentials = tools.run_flow(flow, self.storage, flags)

        self.storage.put(credentials)
        LOG.info("succesfully obtained google credentials")

        return credentials

    def set_search_column(self, header):
        wks = self.worksheet
        cells = wks.findall(str(header))
        cell = False
        for c in cells:
            if c.row == 2:
                cell = c
                break
        if not cell:
            LOG.error("header {} not found in spreadsheet {}".format(header, self.worksheet_name))
            self.error_flag = True
            return
        self.searchcol = cell.col

    def get_data_list(self):
        if self.error_flag: return
        wks = self.worksheet
        mst_list = wks.col_values(1)
        search_list = wks.col_values(self.searchcol)
        data_list = zip(mst_list, search_list)
        return list(data_list)

    def update_field(self, mst, payload):
        wks = self.worksheet
        cells = wks.findall(str(mst))
        cell = False
        for c in cells:
            if c.col == 1:
                cell = c
                break
        if not cell:
            LOG.error("contract {} not found in spreadsheet {}".format(mst, self.worksheet_name))
            return
        row = cell.row
        wks.update_cell(row, self.searchcol, payload)
